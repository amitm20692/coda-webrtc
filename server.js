/*
 * Project:  WebRTC
 * File:      server.js
 * Author:   Amit Mishra (amit.mishra+CODA@gmail.com)
 * 
 * Description:
 * 
 * Revision History:
 *   2020-April-29: 
 * 
 * Copyright (c) 2020 Self
 * 
 * License:
 *    
 */

// I'll share the notes when we  start discussing again so that we can focus
// on code

// so let's create/configure our webserver
// We need 3 things basically
// 1. file server
// 2. https
// 3. web sockets
const fs = require('fs');
const https = require('https');
const webSocket = require('ws');

// So let's configure it one by one
// 1. The server needs to know how to respond to specific URLs

const handleRequests = function(request, response) {

    // By default we'll land on "/", that's when we need our html file to be sent back
    if(request.url === '/') {

        // we need to set headers and then send back the html file
        response.writeHead(200, { "Content-Type": "text/html"});
        response.end(fs.readFileSync("index.html"));

        // We are yet to create the html file
        // we'll do that in a bit
    }
    // We'll also need a js file that will run our webRTC code
    else if (request.url === '/client.js') {
        response.writeHead(200, {"Content-Type" : "application/javascript"});
        response.end(fs.readFileSync("client.js"))
    }

    // Not the best way to handle route, 
    // and please don't use if-else-if ladders :P    
}
// Moving on to the next part
// 2. It needs to know on which IP:Port it needs to listen on
// We need to create the cert and 
// key object first..
const certConfig = {
    key: fs.readFileSync("key.pem"),
    cert: fs.readFileSync("cert.pem")
}

const httpsServer = 
https.createServer(certConfig, handleRequests)
// 3. It needs to know the certifcate
// information to present to browser

// Now we need to tell the port
// our server should listen on
httpsServer.listen(5656, "0.0.0.0");

console.log("listening on: https://localhost:5656")

// the final part
// for our webserver the socket server
const webSocketServer = webSocket.Server;

// this websocket server needs a cofiguration
// like where should it transmit data from
const wss = new webSocketServer({
    server: httpsServer
});

// We'll come back to the connection and
// sending of messages part later

// any questions so far?
// ok so let's write some webRTC code


// here we already have the wss object
// we just need to configure it to send messages
// to clients
// we'll make use of two events
// 1. Connection
// 2. Message

wss.on('connection', (ws) => {

    // now we'll check for message event
    ws.on('message', (message) => {

        // we can simply broadcast it to other clients
        wss.broadcast(message);
    })
});

// Let's write the broadcast function
wss.broadcast = function(message) {
    console.log("server message: ", message)
    
    // webSocketServer always holds all the clients that
    // are connected to it
    // We'll iterate over each client, see it they have their connection
    // open and then simply send the message
    this.clients.forEach((client) => {

        if (client.readyState === webSocket.OPEN) {
            client.send(message);
        }
    });
};

// that's all that is needed for now
// Let's write code for receiving and sending messages
// from client side


// I believe we have done the most part
// let's revisit the notes and see if we missed anything

