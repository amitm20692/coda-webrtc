/*
 * Project:  WebRTC
 * File:      client.js
 * Author:   Amit Mishra (amit.mishra+CODA@gmail.com)
 * 
 * Description:
 * 
 * Revision History:
 *   2020-April-29: 
 * 
 * Copyright (c) 2020 Self
 * 
 * License:
 *    
 */

//#region Props
var localStream;
var peerConnection;
var serverConnection = new WebSocket("wss://localhost:5656");

// Not a great idea, but can work for two peers
// hope so :-)
var myId = Math.round(Math.random() * 31);
// Same url as our server

// We need to listen to message events
// so that we can take action
serverConnection.onmessage = gotMessageFromServer;
//#endregion

// We already have the callback function
function gotMessageFromServer(message) {

    // If this peer hasn't created the peerConnection
    // initiate the connect function
    if (!peerConnection)
        connect(false);

    // This should solve one problem
    // let's test it

    // Messages would be in string format (for us)
    // so let's parse that first
    let data = JSON.parse(message.data);

    console.log("message: ", message.data);

    // We forgot to check for our own id
    if (data.id === myId)
        return;

    // We'll send two types of messages
    // 1. info about ice candidates
    if (data.ice) {

        // Let's add this to peerConnection
        peerConnection
            .addIceCandidate(new RTCIceCandidate(data.ice));
    }

    // 2. info about session
    // sdp => session description protocol
    if (data.sdp) {

        // let's implement this one
        // if we reeive the sdp, we need to store it as
        // remote description
        peerConnection
        .setRemoteDescription(new RTCSessionDescription(data.sdp))
        .then(() => {
            // If we succeed then we'll check if this sdp
            // has an offer and create answer accoringly
            if (data.sdp.type === "offer") {
                peerConnection.createAnswer()
                .then(createDescription) // remember each peer has to know
                // the sdp of others
                .catch((error) => {
                    console.log(error);
                })
            }
        })
        // this message is handle I believe
        // let's write the code for sending these messages
    }
}
//#region Comments
// So here we'll write code about how the clients
// will communicate with each other
// We'll follow some steps 
// 1. getUserMedia - our local A/V
//#endregion
function getMyCam() {

    //#region Comments
    // Remember, the first webRTC object?
    // It's the RTCMediaStream
    // Every browser has its own way of
    // giving you the streams
    //e.g chrome - navigator.getUserMedia.
    // firefox - navigator.mozGetUserMedia etc

    // let's inspect getUserMedia function
    // It requires 3 things
    // 1. constraints - audio, video
    //#endregion
    const constraints = {
        audio: true,
        video: true
    }

    //#region Comments
    // 2. sucessCallbackFn - 
    //  gives back streams/tracks
    // 3. failureCallbackFn - gives back error
    // e.g. User denied access to audio/video
    //#endregion
    navigator.getUserMedia(
        constraints,
        gotLocalStream,
        failedToGetLocalStream
    )
}
// First the successCallback
function gotLocalStream(stream) {

    // Now is the point to create the html file
    // let's take reference of our localVideo element
    let localVideoElement = document.getElementById("localVideo");

    // let's bind the video stream
    localVideoElement.srcObject = stream;

    // let's also hold a reference of this stream
    localStream = stream;

    console.log("localStream: ", stream);
}

// Now the failure function
function failedToGetLocalStream(error) {

    // we can simply log the error
    console.log(error);
}

// All good so far

//#region Comments
// for this part, we'll need a connect button
// so that we can initiate the process
// so let's say the button on html calls
// connect function
//#endregion
function connect(shouldInitiateOffer) {

    //#region Comments
    // 2. Create a RTCPeerconncection 
    // let's hold a reference of this connection
    // It takes a configuration object
    // which can have info about STUN and TURN servers
    // for now, we don't need them
    // but we'll talk about them
    //#endregion
    peerConnection = new RTCPeerConnection(null);

    //#region Comments 
    // 3. Add our local streams (audio and video )
    //     to the peer connection object
    // This is the same object wew got back from
    // getUserMedia call
    //#endregion
    peerConnection.addStream(localStream);

    //#region Comments
    // 4. We'll look for any remote streams / tracks
    //      added by other peers
    // We'll implement this callback function
    // this will give us remote stream from our peers 
    //#endregion
    peerConnection.ontrack = gotRemoteTrack;

    //#region Comments
    // 5. We'll look for ice candidates 
    //      (IP:Port and protocol info)
    // This will again be a callback function
    // We'll implement that.. soon..
    //#endregion
    peerConnection.onicecandidate = gotIceCandidate;

    //#region Comments
    // 6. We'll create offer for the clients
    //      so that they can connect to us
    //#endregion
    
    if (!shouldInitiateOffer)
        return;

    peerConnection.createOffer()
        .then(createDescription)
        // on success we need to store this info
        // we'll write a callbackFn
        .catch((error) => {

            // this can be another function
            // but let's use this one
            console.log(error);
        })

}
// What to do when we receive remote streams?
// the first call back
function gotRemoteTrack(event) {

    console.log("Remote stream", event.streams);
    // we need a remoteVideo tag, so that we can show
    // it separately, going back to html file

    let remoteVideoElement = document.getElementById("remoteVideo");

    //#region Comments
    // Let's attach the remote stream
    // Every media stream will give us an array
    // of tracks, for now we are interested in only
    // one of them, so we'll take first one
    //#endregion
    remoteVideoElement.srcObject = event.streams[0];
}

// Second call back
function gotIceCandidate(event) {

    //#region Comments
    // There might be instances when we don't receive
    // any IP:Port combination, so let's not log that
    //#endregion
    if (!event.candidate)
        return;

    console.log("candidate: ", event.candidate);

    // We'll need to send this information to remote peers
    // let's come back to this once we create 
    // websockets on client side
    // we'll also need an id, so that we don't act on
    // our own
    serverConnection.send(JSON.stringify(
        { "ice": event.candidate, "id": myId }
    ));

}

// third callback
function createDescription(description) {

    // This is our own information as well as the
    // peer info
    // so let's set this info
    peerConnection.setLocalDescription(description)
        .then(() => {

            // here we have to send this info
            // to server
            serverConnection.send(JSON.stringify(
                {
                    "sdp": peerConnection.localDescription,
                    "id": myId
                }
            ))
            // we havent's defined the myId yet
        })
        .catch((error) => {
            console.log(error);
        })
}

// 7 . Finally, the answer to whatever offer
//       we get
// the final part we'll get back to,
// once we implement all the callbacks we used just now




